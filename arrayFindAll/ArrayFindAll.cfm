<!---
		create by:		jhon Edison Gomez
		date:			10-Apr-2015
		description:	Examples for arrayFindAll function
--->

<!---
		this example is to find a username in an array
		this function return an array with the index position
		in the array
--->

<cfscript>

	/*this is the array with the work in the examples*/
	array = ['jhonSupport','monicaSupport','juanSupport'];

	//first example
	newArray = arrayFindAll(array,'monicaSupport');
	writedump(newArray);


	//second example with a callback function
	newArray = arrayFindAll(array,function(j){

		//the element to find should be in the before array
		result = find("jhonSupport",j);

		if(result > 0 ){
			return true;

		}else{
			return false;
		}
	});
	writedump(newArray);

</cfscript>





